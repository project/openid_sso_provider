<?php

/**
 * @file
 *
 * Plugin to provide a node context. A relying_party context is a relying_party wrapped in a
 * context object that can be utilized by anything that accepts contexts.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
/*$plugin = array(
  'title' => t("Relying party . AA"),
  'description' => t('A relying party object.'),
  //'context' => 'openid_sso_provider_selection_context_create_relying_party',
  //'edit form' => 'openid_sso_provider_selection_context_relying_party_settings_form',
  'keyword' => 'openid_sso_provider_rps',
  'required context' => new ctools_context_required(t('Relying party'), 'openid_sso_provider_rps'),
  //'context name' => 'openid_sso_provider_rps',
  //'convert list' => 'openid_sso_provider_selection_context_relying_party_convert_list',
  //'convert' => 'openid_sso_provider_selection_context_relying_party_convert',
  //'placeholder form' => array(
  //  '#type' => 'textfield',
  //  '#description' => t('Enter the node ID of a node for this context.'),
  //),
);
*/
